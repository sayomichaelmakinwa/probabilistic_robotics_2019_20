# Distance Map example

# Requirements

* `build-essentials`
* `openCV`
* `cmake`

# Installation

Create a `build` directory and move inside it

* `mkdir build; cd build`

compile

* `cmake ..`
* `make`

# Run

* `./distance_map_test`
